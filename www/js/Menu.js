define(['phaser'], function (Phaser) {
    'use strict';

    function Menu(game) {
    }

    Menu.prototype.constructor = Menu;

    Menu.prototype.init = function () {
        window.embaixadinha = window.embaixadinha || {};
        window.embaixadinha.config = this.game.cache.getJSON('config');
    };

    Menu.prototype.create = function () {        
        // start bgm
        this._bgmIntro = this.game.add.audio('bgm_intro');
        this._bgmIntro.loopFull();

        // add sfx for menu selection
        this._sfxSelection = this.game.add.audio('sfx_selection');

        // add background
        this._bg = this.game.add.image(0, 0, 'bg_splash');

        // add foreground
        this._logo = this.game.add.image(this.game.width / 2, 120, 'logo');
        this._btnPlay = this.game.add.button(this.game.width / 2, this.game.height / 2 + 140, 'btn_play', this.btnPlayClick, this, null, null, 1, 0);
        this._btnChallenge = this.game.add.button(this._btnPlay.x, this._btnPlay.bottom, 'btn_challenge', this.btnChallengeClick, this, null, null, 1, 0);

        // config foreground elements
        this._logo.anchor.set(0.5);
        this._btnPlay.anchor.set(0.5);
        this._btnChallenge.anchor.set(0.5);

        // mute volume
        this._isMuted = (Number(localStorage.getItem('isMuted')) || 0) && 1;

        // add sound controller
        this._btnSoundOn = this.game.add.button(378, 10, '', this.btnSoundOnClick, this);

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
        }
    };

    Menu.prototype.btnPlayClick = function () {
        localStorage.setItem('gameMode', '0');
        this._sfxSelection.play();
        this._bgmIntro.destroy();
        this.game.state.start('Game');
    };

    Menu.prototype.btnChallengeClick = function () {
        var self = this;

        localStorage.setItem('gameMode', '1');
        this._sfxSelection.play();
        this.loading();

        window.plugins.playGamesServices.isSignedIn(function (result) {
            if (!result.isSignedIn) {
                window.plugins.playGamesServices.auth(function () {
                    self.game.state.start('Game');
                }, function () {
                    navigator.notification.alert('Falha de autenticação nos serviços do Google Play! Tente novamente!', null, 'Erro!', 'OK');
                    self.game.state.start('Menu');
                });
            } else {
                self.game.state.start('Game');
            }
        }, function () {
            navigator.notification.alert('Não foi possível verificar o status da conexão! Tente novamente!', null, 'Erro!', 'OK');
            self.game.state.start('Menu');
        });
    };

    Menu.prototype.loading = function () {
        // destroy all screen components
        this._bg.destroy();
        this._bgmIntro.destroy();
        this._btnPlay.destroy();
        this._btnChallenge.destroy();
        this._btnSoundOn.destroy();
        this._logo.destroy();
        this._sfxSelection.destroy();

        // set backscreen color
		this.game.stage.backgroundColor = '#000';

        // load the loading spin
        var loading = this.game.add.image(this.game.width / 2, this.game.height / 2, 'loading');
        loading.anchor.set(0.5);
        loading.scale.set(0.2);
        loading.angle = 0;

        // rotate the loading spin
        this.game.add.tween(loading).to({ angle: 360 }, 1000, Phaser.Easing.Linear.None, true, 0, -1, false);

        // add label loading text
        var title = this.game.add.text(this.game.width / 2, this.game.height / 2 + 100, 'Aguarde...', { fill: '#fff' });
        title.anchor.set(0.5);
    };

    Menu.prototype.btnSoundOnClick = function () {
        this._isMuted = !this._isMuted;
        this.game.sound.mute = this._isMuted;

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
            localStorage.setItem('isMuted', '1');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
            localStorage.setItem('isMuted', '0');
        }
    };

	return Menu;
});