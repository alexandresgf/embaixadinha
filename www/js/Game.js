define(['phaser'], function (Phaser) {
	'use strict';

	function Game (game) {
    }

    Game.prototype.constructor = Game;

    Game.prototype.create = function () {
        // google analytics track game screen
		window.analytics.trackView('Embaixadinha - Game Screen');

        // start arcade physics
	    this.game.physics.startSystem(Phaser.Physics.ARCADE);

	    // set gravity on y-axis
	    this.game.physics.arcade.gravity.y = 2600;

	    // add background
	    this.game.add.image(0, 0, 'bg_game');

        // add effects
        this._effectExplosion = this.game.add.sprite(0, 0, 'explosion');

        // setup effects
        this._effectExplosion.alpha = 0;
        this._effectExplosion.anchor.set(0.5);

        // add animations
        this._animExplosion = this._effectExplosion.animations.add('ball_transition');

        // add outside walls
        this._wallLeft = this.game.add.sprite(-32, 0, 'wall');
        this._wallRight = this.game.add.sprite(this.game.width, 0, 'wall');

        // setup left wall
        this.game.physics.enable(this._wallLeft, Phaser.Physics.ARCADE);
        this._wallLeft.body.allowGravity = false;
        this._wallLeft.body.immovable = true;

        // setup right wall
        this.game.physics.enable(this._wallRight, Phaser.Physics.ARCADE);
        this._wallRight.body.allowGravity = false;
        this._wallRight.body.immovable = true;

        // add bgm sounds
        this._bgmIntro = this.game.add.audio('bgm_intro');
        this._bgmStart = this.game.add.audio('bgm_start');
        this._bgmChallenge = this.game.add.audio('bgm_challenge');

	    // add sound effects
	    this._sfxHitPaperBall = this.game.add.audio('sfx_hit_paperball');
        this._sfxHitLightBall = this.game.add.audio('sfx_hit_lightball');
        this._sfxHitMediumBall = this.game.add.audio('sfx_hit_mediumball');
        this._sfxHitHeavyBall = this.game.add.audio('sfx_hit_heavyball');
        this._sfxExplosion = this.game.add.audio('sfx_explosion');

        // current ball
        this._currBall = 4;

        // kick counter
        this._kickCount = 0;

        // time for the next kick
        this._kickTimer = 0;

        // kick power
        this._kickPow = 1000;

        // record
        this._record = 0;

        // game started flag
        this._isGameStarted = false;

        // mute volume
        this._isMuted = (Number(localStorage.getItem('isMuted')) || 0) && 1;

        // add sound controller
        this._btnSoundOn = this.game.add.button(378, 10, '', this.btnSoundOnClick, this);

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
        }

        // create the ball
        this._ball = this.game.add.sprite(this.game.width / 2, this.game.height, window.embaixadinha.config.ball[this._currBall].sprite);

        // make it invisible until the game start
        this._ball.alpha = 0;

        // game mode
        this._gameMode = Number(localStorage.getItem('gameMode'));

        // current record
        if (this._gameMode) {
            this.initChallengeMode();
        } else {
            this.initLocalMode();
        }
    };

	Game.prototype.update = function () {
        if (this._isGameStarted) {
            // check side wall collision
            this.game.physics.arcade.collide(this._ball, this._wallLeft);
            this.game.physics.arcade.collide(this._ball, this._wallRight);

            // check if is game over
            if (this._ball.body.position.y > this.game.height) {
                this.save();
            }
        }
	};

    Game.prototype.initChallengeMode = function() {
        var self = this;

        window.plugins.playGamesServices.isSignedIn(function (result) {
            if (result.isSignedIn) {
                if (localStorage.getItem('first_time') === null) {
                    var leaderboardId = window.embaixadinha.config.leaderboardId;

                    window.plugins.playGamesServices.getPlayerScore({ leaderboardId: leaderboardId }, function (data) {
                        if (data.playerScore > 0) {
                            localStorage.setItem('challenge_record', String(data.playerScore));
                            self.selectBall();
                            self.showHUD();
                            self.startUp();
                        } else {
                            var achievementId = window.embaixadinha.config.achievement[0];

                            window.plugins.playGamesServices.unlockAchievementNow({ achievementId: achievementId }, function () {
                                localStorage.setItem('first_time', '1');
                                self.selectBall();
                                self.showHUD();
                                self.startUp();
                            }, function () {
                                navigator.notification.alert('Falha nos serviços do Google Play! Tente novamente!', null, 'Erro!', 'OK');
                                self.game.state.start('Menu');
                            });
                        }
                    }, function (err) {
                        navigator.notification.alert(err, null, 'Erro!', 'OK');
                        self.game.state.start('Menu');
                    });
                } else {
                    self.selectBall();
                    self.showHUD();
                    self.startUp();
                }
            } else {
                navigator.notification.alert('Você não está logado no Google Play! Tente novamente!', null, 'Erro!', 'OK');
                self.game.state.start('Menu');
            }
        }, function () {
            navigator.notification.alert('Não foi possível verificar o status da conexão! Tente novamente!', null, 'Erro!', 'OK');
            self.game.state.start('Menu');
        });
    };

    Game.prototype.initLocalMode = function () {
        this._record = Number(localStorage.getItem('local_record')) || 0;
        this.showHUD();
        this.startUp();
    };

    Game.prototype.startUp = function () {
        // play bgm start
        this._bgmStart.play();
        this._bgmStart.onStop.add(function () {
            // setup the ball
            this.game.physics.enable(this._ball, Phaser.Physics.ARCADE);
            this._ball.anchor.set(0.5);
            this._ball.inputEnabled = true;
            this._ball.body.bounce.set(1);
            this._ball.body.angularDrag = 50;
            this._ball.events.onInputDown.add(this.kickUp, this);

            // setup animation listener
            this._animExplosion.onComplete.add(function () {
                this._effectExplosion.alpha = 0;
                this._ball.loadTexture(window.embaixadinha.config.ball[this._currBall].sprite);
            }, this);

            // hud z-order
            this._hudKickCount.bringToTop();
            this._hudKickTop.bringToTop();
            this._btnSoundOn.bringToTop();

            // make the ball visible
            this._ball.alpha = 1;

            // start the game
            var start = this.game.add.tween(this._ball.body.velocity).to({ y: -1300 }, 100, Phaser.Easing.Linear.None, true);
            start.onComplete.add(function () {
                // start challenge mode bgm
                if (this._gameMode) {
                    this._bgmChallenge.loopFull();
                }

                // allow game logic
                this._isGameStarted = true;
            }, this);
        }, this);
    };

    Game.prototype.showHUD = function () {
        // add HUD
        this._hudKickCount = this.game.add.image(0, 5, 'hud_kickcount');
        this._hudKickTop = this.game.add.image(this.game.width / 2 + 40, 10, 'hud_kicktop');

        // add kick counter title
        var fontScore = { font: '75px Fixedsys', fill: '#000', align: 'center' };
        this._kickCountTitle = this.game.add.text(25, 20, '0000', fontScore);
        this._hudKickCount.addChild(this._kickCountTitle);

        // add kick top title
        var fontRecord = { font: '50px Fixedsys', fill: '#000', align: 'center' };
        this._kickTopTitle = this.game.add.text(1, 10, this.formatScore(this._record), fontRecord);
        this._hudKickTop.addChild(this._kickTopTitle);
    };

    Game.prototype.selectBall = function () {
        this._record = Number(localStorage.getItem('challenge_record')) || 0;

        for (var i = 0; i < window.embaixadinha.config.ball.length; i++) {
            if (i === window.embaixadinha.config.ball.length - 1 && this._record >= window.embaixadinha.config.ball[i].score) {
                this._ball.loadTexture(window.embaixadinha.config.ball[i].sprite);
                this._currBall = i;
            } else if (this._record >= window.embaixadinha.config.ball[i].score && this._record < window.embaixadinha.config.ball[i + 1].score) {
                this._ball.loadTexture(window.embaixadinha.config.ball[i].sprite);
                this._currBall = i;
            }
        }
    };

	Game.prototype.kickUp = function () {
        var now = this.game.time.now;

		if (now > this._kickTimer) {
            // check which sfx will play
            switch (window.embaixadinha.config.ball[this._currBall].sfx) {
                case 'sfx_hit_paperball':
                    this._sfxHitPaperBall.play();
                    break;

                case 'sfx_hit_lightball':
                    this._sfxHitLightBall.play();
                    break;

                case 'sfx_hit_mediumball':
                    this._sfxHitMediumBall.play();
                    break;

                case 'sfx_hit_heavyball':
                    this._sfxHitHeavyBall.play();
                    break;

                default:
                    this._sfxHitMediumBall.play();
                    break;
            }

            this._kickCount++;

            if (this._gameMode && this._currBall < window.embaixadinha.config.ball.length - 1) {
                if (this._kickCount >= window.embaixadinha.config.ball[this._currBall + 1].score) {
                    this._currBall++;
                    this._effectExplosion.x = this._ball.x;
                    this._effectExplosion.y = this._ball.y;
                    this._effectExplosion.alpha = 1;
                    this._animExplosion.play();
                    this._sfxExplosion.play();
                    window.plugins.playGamesServices.unlockAchievementNow({ achievementId: window.embaixadinha.config.achievement[this._currBall] });
                }
            }

            if (this._kickCount === 10000) {
                this._kickCount = 9999;
            }

            this._kickCountTitle.setText(this.formatScore(this._kickCount));

            var angle = this.game.physics.arcade.angleToXY(this._ball, this.game.input.x, this.game.input.y);
            angle = this.game.math.normalizeAngle(angle);

            var cos = Math.cos(angle);
            var sin = Math.sin(angle);

            this._ball.body.angularVelocity = (this._kickPow + window.embaixadinha.config.ball[this._currBall].kickMod) * cos * -1;
            this._ball.body.velocity.x = (this._kickPow + window.embaixadinha.config.ball[this._currBall].kickMod) * cos * -1;
			this._ball.body.velocity.y = -(this._kickPow + window.embaixadinha.config.ball[this._currBall].kickMod) * Math.abs(sin);
			this._kickTimer = now + 500;
		}
	};

	Game.prototype.save = function () {
        localStorage.setItem('score', String(this._kickCount));
        this._bgmChallenge.destroy();
        this.game.state.start('GameOver');
	};

    Game.prototype.formatScore = function (score) {
        var formatedScore;

        if (score < 10) {
            formatedScore = '000' + String(score);
        } else if (score >= 10 && score < 100) {
            formatedScore = '00' + String(score);
        } else if (score >= 100 && score < 1000) {
            formatedScore = '0' + String(score);
        } else {
            formatedScore = String(score);
        }

        return formatedScore;
    };

    Game.prototype.btnSoundOnClick = function () {
        this._isMuted = !this._isMuted;
        this.game.sound.mute = this._isMuted;

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
            localStorage.setItem('isMuted', '1');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
            localStorage.setItem('isMuted', '0');
        }
    };

    return Game;
});