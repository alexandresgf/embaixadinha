define(['phaser'], function (Phaser) {
	'use strict';

	function Ads (game) {
        // listen when ad ir ready
        document.addEventListener('onAdLoaded', function (e) {
            AdMob.showInterstitial();
        });

		// listen when ad is closed
		document.addEventListener('onAdDismiss', function (e) {
			var quit = Number(localStorage.getItem('quit'));

			if (quit) {
				game.state.start('Menu');
            } else {
				game.state.start('Game');
            }
		});

        // listen if something fail
        document.addEventListener('onAdFailLoad', function (e) {
            console.error(e.error + ': ' + e.reason);
            var quit = Number(localStorage.getItem('quit'));

			if (quit) {
				game.state.start('Menu');
            } else {
				game.state.start('Game');
            }
        });
	}

	Ads.prototype.constructor = Ads;

	Ads.prototype.create = function () {
		// set backscreen color
		this.game.stage.backgroundColor = '#000';

        // load the loading spin
        var loading = this.game.add.image(this.game.width / 2, this.game.height / 2, 'loading');
        loading.anchor.set(0.5);
        loading.scale.set(0.2);
        loading.angle = 0;

        // rotate the loading spin
        this.game.add.tween(loading).to({ angle: 360 }, 1000, Phaser.Easing.Linear.None, true, 0, -1, false);

        // add label loading text
        var title = this.game.add.text(this.game.width / 2, this.game.height / 2 + 100, 'Aguarde...', { fill: '#fff' });
        title.anchor.set(0.5);

        // start admob
        if (AdMob) {
            AdMob.prepareInterstitial({
                adId: 'ca-app-pub-7403543083567100/4353133078',
                autoShow: false,
                isTesting: false,
                overlap: true
            });
        }
	};

	return Ads;
});