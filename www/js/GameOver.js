define(['phaser'], function (Phaser) {
	'use strict';

	function GameOver (game) {
	}

	GameOver.prototype.constructor = GameOver;

	GameOver.prototype.create = function () {
		// add background
		this.game.add.image(0, 0, 'bg_gameover');

        // mute volume
        this._isMuted = (Number(localStorage.getItem('isMuted')) || 0) && 1;

        // add sound controller
        this._btnSoundOn = this.game.add.button(378, 10, '', this.btnSoundOnClick, this);

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
        }

        // load bgm game over
        this._bgmGameOver = this.game.add.audio('bgm_gameover');

        // game mode
        this._gameMode = Number(localStorage.getItem('gameMode'));

        // load score
		var score = Number(localStorage.getItem('score')) || 0;

        // HUD
        this._hudFinalScore = this.game.add.image(this.game.width / 2, this.game.height / 2, 'hud_finalscore');
        this._hudFinalScore.anchor.set(0.5);

        // add score to HUD
        var fontScore = { font: '140px Fixedsys', fill: '#000', align: 'center' };
        this._scoreTitle = this.game.add.text(76, 355, this.formatScore(score), fontScore);

		// add buttons
        this._btnShare = this.game.add.button(0, 10, 'btn_share', this.share, this);
		this._btnExit = this.game.add.button(7, this.game.height - 100, 'btn_exit', this.btnExitClick, this);
        this._btnNewGame = this.game.add.button(321, this._btnExit.y, 'btn_newgame', this.btnNewGameClick, this);

        // check challenge mode elements and record submission
        if (this._gameMode) {
            var self = this;
            var record = Number(localStorage.getItem('challenge_record')) || 0;

            if (score > record) {
                window.plugins.playGamesServices.isSignedIn(function (result) {
                    if (result.isSignedIn) {
                        var leaderboardId = window.embaixadinha.config.leaderboardId;
                        
                        window.plugins.playGamesServices.submitScore({ score: score, leaderboardId: leaderboardId }, function () {
                            localStorage.setItem('challenge_record', String(score));
                        }, function () {
                            navigator.notification.alert('Não foi possível conectar ao Google Play! Você não poderá continuar neste modo de jogo!', null, 'Erro!', 'OK');
                            self.showAds(1);
                        });
                    } else {
                        navigator.notification.alert('Erro de login! Você não poderá continuar neste modo de jogo!', null, 'Erro!', 'OK');
                        self.showAds(1);
                    }
                }, function () {
                    navigator.notification.alert('Não foi possível verificar o status da conexão! Você não poderá continuar neste modo de jogo!', null, 'Erro!', 'OK');
                    self.showAds(1);
                });
            }

            this._btnAchiev = this.game.add.button(this._btnExit.right, this._btnExit.y, 'btn_achiev', this.btnAchievClick, this);
            this._btnScore = this.game.add.button(this._btnAchiev.right, this._btnAchiev.y, 'btn_score', this.btnScoreClick, this);
        } else {
            var record = Number(localStorage.getItem('local_record')) || 0;

            if (score > record) {
                localStorage.setItem('local_record', String(score));
            }
        }

        // play game over song
        this._bgmGameOver.play();
	};

	GameOver.prototype.btnNewGameClick = function () {
        this._bgmGameOver.destroy();
        this.showAds(0);
	};

	GameOver.prototype.btnExitClick = function () {
        this._bgmGameOver.destroy();
		this.showAds(1);
	};

    GameOver.prototype.btnAchievClick = function () {
        window.plugins.playGamesServices.showAchievements();
	};

    GameOver.prototype.btnScoreClick = function () {
        window.plugins.playGamesServices.showLeaderboard({ leaderboardId: window.embaixadinha.config.leaderboardId });
	};

    GameOver.prototype.share = function () {
        facebookConnectPlugin.showDialog(
            {
                method: 'share',
                href: 'https://play.google.com/store/apps/details?id=com.brofistcorp.embaixadinha'
            },
            function (data) {
                console.log(data);
            },
            function (data) {
                console.error(data);
            });
	};

    GameOver.prototype.showAds = function (quit) {
        var showAdsCounter = Number(localStorage.getItem('showAdsCounter'));
		var newGameCounter = Number(localStorage.getItem('newGameCounter'));

        if (quit) {
            localStorage.setItem('quit', String(quit));
		    this.game.state.start('Ads');
        } else if (newGameCounter === showAdsCounter) {
            localStorage.setItem('quit', String(quit));
            localStorage.setItem('showAdsCounter', String(this.game.rnd.integerInRange(5, 10)));
            localStorage.setItem('newGameCounter', '0');
            this.game.state.start('Ads');
        } else {
            localStorage.setItem('newGameCounter', String(++newGameCounter));
            this.game.state.start('Game');
        }
    };

    GameOver.prototype.btnSoundOnClick = function () {
        this._isMuted = !this._isMuted;
        this.game.sound.mute = this._isMuted;

        if (this._isMuted) {
            this._btnSoundOn.loadTexture('btn_soundoff');
            localStorage.setItem('isMuted', '1');
        } else {
            this._btnSoundOn.loadTexture('btn_soundon');
            localStorage.setItem('isMuted', '0');
        }
    };

    GameOver.prototype.formatScore = function (score) {
        var formatedScore;

        if (score < 10) {
            formatedScore = '000' + String(score);
        } else if (score >= 10 && score < 100) {
            formatedScore = '00' + String(score);
        } else if (score >= 100 && score < 1000) {
            formatedScore = '0' + String(score);
        } else {
            formatedScore = String(score);
        }

        return formatedScore;
    };

	return GameOver;
});