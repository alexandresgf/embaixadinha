define(['phaser'], function (Phaser) {
    'use strict';
    
    function Preloader (game) {
    }
    
    Preloader.prototype.constructor = Preloader;
    
    Preloader.prototype.preload = function () {
        // load configuration file
        this.load.json('config', 'assets/data/config.json');

	    // load sfx
	    this.load.audio('sfx_hit_heavyball', 'assets/sound/sfx/sfx_hit_heavyball.ogg');
        this.load.audio('sfx_hit_mediumball', 'assets/sound/sfx/sfx_hit_mediumball.ogg');
        this.load.audio('sfx_hit_lightball', 'assets/sound/sfx/sfx_hit_lightball.ogg');
        this.load.audio('sfx_hit_paperball', 'assets/sound/sfx/sfx_hit_paperball.ogg');
        this.load.audio('sfx_selection', 'assets/sound/sfx/sfx_selection.ogg');
        this.load.audio('sfx_explosion', 'assets/sound/sfx/sfx_explosion.ogg');

        // load bgm
	    this.load.audio('bgm_intro', 'assets/sound/bgm/bgm_intro.ogg');
        this.load.audio('bgm_start', 'assets/sound/bgm/bgm_start.ogg');
        this.load.audio('bgm_gameover', 'assets/sound/bgm/bgm_gameover.ogg');
        this.load.audio('bgm_challenge', 'assets/sound/bgm/bgm_challenge.ogg');

        // load backgrounds
        this.load.image('bg_game', 'assets/img/bg/bg_game.png');
        this.load.image('bg_gameover', 'assets/img/bg/bg_gameover.png');
        this.load.image('bg_splash', 'assets/img/bg/bg_splash.png');
        
        // load balls
        this.load.image('ball_paper', 'assets/img/ball/ball_01_paper.png');
        this.load.image('ball_can', 'assets/img/ball/ball_02_can.png');
        this.load.image('ball_milk', 'assets/img/ball/ball_03_milk.png');
        this.load.image('ball_leather', 'assets/img/ball/ball_04_leather.png');
        this.load.image('ball_default', 'assets/img/ball/ball_05_default.png');
        this.load.image('ball_bronze', 'assets/img/ball/ball_06_bronze.png');
        this.load.image('ball_silver', 'assets/img/ball/ball_07_silver.png');
        this.load.image('ball_gold', 'assets/img/ball/ball_08_gold.png');
        this.load.image('ball_platinum', 'assets/img/ball/ball_09_platinum.png');
        this.load.image('ball_diamond', 'assets/img/ball/ball_10_diamond.png');
        
        // load misc
        this.load.image('logo', 'assets/img/misc/logo.png');
        this.load.image('wall', 'assets/img/misc/wall.png');
        this.load.image('loading', 'assets/img/misc/loading.png');

        // load effects
        this.load.spritesheet('explosion', 'assets/img/effect/explosion.png', 155, 170);

        // load HUD
        this.load.image('hud_kickcount', 'assets/img/hud/hud_kickcount.png');
        this.load.image('hud_kicktop', 'assets/img/hud/hud_kicktop.png');
        this.load.image('hud_finalscore', 'assets/img/hud/hud_finalscore.png');

        // load buttons
	    this.load.image('btn_newgame', 'assets/img/hud/btn_newgame.png');
	    this.load.image('btn_exit', 'assets/img/hud/btn_exit.png');
        this.load.image('btn_soundoff', 'assets/img/hud/btn_soundoff.png');
        this.load.image('btn_soundon', 'assets/img/hud/btn_soundon.png');
        this.load.image('btn_share', 'assets/img/hud/btn_share.png');
        this.load.image('btn_achiev', 'assets/img/hud/btn_achiev.png');
        this.load.image('btn_score', 'assets/img/hud/btn_score.png');
        this.load.spritesheet('btn_play', 'assets/img/hud/btn_play.png', 345, 108);
        this.load.spritesheet('btn_challenge', 'assets/img/hud/btn_challenge.png', 345, 108);
    };
    
    Preloader.prototype.create = function () {
        this.game.state.start('Menu');
    };
    
    return Preloader;
});